﻿namespace BackEnd.Responses
{
    public class GeneralCountry
    {
        public long CountryId { get; set; }
        public string CountryName { get; set; }
    }

   public class GeneralListCountry
    {
        public List<GeneralCountry> Country { get; set; }
    }
}
