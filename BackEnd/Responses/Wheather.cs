﻿namespace BackEnd.Responses
{
    public class Wheather
    {

    }

    public class Coord
    {
        public double Lon { get; set; }
        public double Lat { get; set; }
    }

    public class Weather
    {
        public long Id { get; set; }
        public string Main { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
    }

    public class Main
    {
        public double Temp { get; set; }
        public double Feels_Like { get; set; }
        public double Temp_Min { get; set; }
        public double Temp_Max { get; set; }
        public long Pressure { get; set; }
        public long Humidity { get; set; }
        public long Sea_Level { get; set; }
        public long Grind_Level { get; set; }
    }

    public class Wind
    {
        public double Speed { get; set; }
        public double Deg { get; set; }
        public double Gust { get; set; }
    }

    public class Clouds
    {
        public long All { get; set; }
    }

    public class Sys
    {
        public long Type { get; set; }
        public long Id { get; set; }
        public string Country { get; set; }
        public long Sunrise { get; set; }
        public long sunset { get; set; }
    }


    public class ResponsesWeather
    {
        public Location Location { get; set; }
        public string Time { get; set; }
        public Wind Wind { get; set; }
        public long Visibility { get; set; }
        public Clouds Clouds { get; set; }

        public long Pressure { get; set; }
        public long Humidity { get; set; }
        public List<Weather> Wheather { get; set; }

        public Temperature Temperature { get; set; }

        public string WeatherMain { get; set; }
        public string WeatherDesc { get; set; }


    }
    public class Location
    {
        public Coord Coord { get; set; }

        public string Country { get; set; }
        public string Name { get; set; }


    }

    public class Temperature
    {
        public double TempCelcius { get; set; }
        public double TempFahrenheit { get; set; }
        public double TempMinCelcius { get; set; }
        public double TempMaxCelcius { get; set; }

        public double TempMinFahrenheit { get; set; }
        public double TempMaxFahrenheit { get; set; }
    }

    public class GeneralWeather
    {
        public Coord Coord { get; set; }

        public List<Weather> Weather { get; set; }

        public Main Main { get; set; }

        public Wind Wind { get; set; }
        public Clouds Clouds { get; set; }

        public Sys Sys { get; set; }
        public string Base { get; set; }
        public long Visibility { get; set; }
        public long Dt { get; set; }
        public long Timezone { get; set; }
        public long Id { get; set; }
        public string Name { get; set; }
        public long Cod { get; set; }
    }
}


