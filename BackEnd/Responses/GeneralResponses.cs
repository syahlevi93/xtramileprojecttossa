﻿namespace BackEnd.Responses
{
    public class GeneralResponses
    {
        public bool Error { get; set; }
        public string Message { get; set; }
        public object Data { get; set; }
    }
}
