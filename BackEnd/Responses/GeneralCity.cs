﻿namespace BackEnd.Responses
{
    public class GeneralCity
    {
        public long CityId { get; set; }
        public long CountryId { get; set; }
        public string CityName { get; set; }
    }

    public class GeneralListCity
    {
        public List<GeneralCity> City { get; set; }
    }
}
