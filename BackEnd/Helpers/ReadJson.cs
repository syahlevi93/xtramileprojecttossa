﻿using BackEnd.Responses;
using Newtonsoft.Json;

namespace BackEnd.Helpers
{
    public class ReadJson
    {
        public async Task<(GeneralListCountry Data, bool cek)>ReadJsonCountry()
        {
            using (StreamReader r = new StreamReader(@"ListCountry.json"))
            {
                string json = r.ReadToEnd();
                var items = await (Task.Run(()=> JsonConvert.DeserializeObject<GeneralListCountry>(json)));
                //ListOfCity.Add(items);
                return (items,false);
            }

        }

        public async Task<(GeneralListCity Data, bool cek)> ReadJsonCity()
        {
            using (StreamReader r = new StreamReader(@"ListCity.json"))
            {
                string json = r.ReadToEnd();
                var items = await (Task.Run(() => JsonConvert.DeserializeObject<GeneralListCity>(json)));
                //ListOfCity.Add(items);
                return (items, false);
            }

        }
    }
}
