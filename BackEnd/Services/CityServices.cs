﻿using BackEnd.Responses;
using Newtonsoft.Json;
using BackEnd.Helpers;
using System.Net.Http.Headers;

namespace BackEnd.Services
{
    public class CityServices
    {

        ReadJson _readjson = new ReadJson();

        public async Task<(bool error, string message, GeneralListCountry data)> GetCity()
        {
            try
            {
                var _Data = await _readjson.ReadJsonCountry();
                return (false, "OK", _Data.Data);
            }
            catch(Exception ex)
            {
                return (true, ex.Message, null);
            }
        }

        public async Task<(bool error, string message)> AddNewCountry(string Country)
        {
            try
            {
                var _Data = await _readjson.ReadJsonCountry();
                var getMax = _Data.Data.Country.Select(es => es.CountryId).Max();
                var InsertData = new List<GeneralCountry>();
                var listData = new GeneralCountry()
                {
                    CountryId=getMax+1,
                    CountryName=Country
                };

                InsertData.Add(listData);
                var datax = JsonConvert.SerializeObject(InsertData);
                    File.WriteAllText(@"ListCountry.json",datax);
                
                    return (false, "OK");
            }
            catch (Exception ex)
            {
                return (true, ex.Message);
            }
        }

        public async Task<(bool error, string message, ResponsesWeather data)> GetWeatherByCity(string City)

        {

            var Urls = "http://api.openweathermap.org/";
            var APPId = "4f8766fa9054077e54a88bc6baf3a07f";
            try
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri(Urls);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    //GET Method
                    HttpResponseMessage response = await client.GetAsync("data/2.5/weather?q=" + City + "&APPID=" + APPId);
                    if (response.IsSuccessStatusCode)
                    {
                        var Data = await response.Content.ReadFromJsonAsync<GeneralWeather>();
                        var locations = new Location()
                        {
                            Coord=Data.Coord,
                            Name=Data.Name,
                            Country=Data.Sys.Country

                        };

                        var Temp = new Temperature()
                        {
                            TempCelcius=Data.Main.Temp-273.15,
                            TempMaxCelcius=Data.Main.Temp_Max-273.15,
                            TempMinCelcius=Data.Main.Temp_Min-273.15,
                            TempFahrenheit=(Data.Main.Temp-273.15)*1.8+32,
                            TempMaxFahrenheit=(Data.Main.Temp_Max-273.15)*1.8+32,
                            TempMinFahrenheit=(Data.Main.Temp_Min-273.15)*1.8+32

                        };

                        var DataResponses = new ResponsesWeather()
                        {
                            Wind=Data.Wind,
                            Location=locations,
                            Clouds=Data.Clouds,
                            Visibility=Data.Visibility,
                            Pressure=Data.Main.Pressure,
                            Humidity=Data.Main.Humidity,
                            Temperature=Temp,
                            Wheather=Data.Weather,
                            Time=DateTime.Now.Date.ToString("dd/MMMM/yyyy hh:mm:ss"),
                            WeatherMain=Data.Weather.Select(es=>es.Main).FirstOrDefault(),
                            WeatherDesc= Data.Weather.Select(es => es.Description).FirstOrDefault()
                        };

                        return (false, "OK", DataResponses);
                    }
                    else
                    {
                        return (true, "Error", null);
                    }


                }
            }

            catch (Exception ex)
            {
                return (true, ex.Message, null);

            }



        }
        public async Task<(bool error, string message, List<GeneralCity> data)> GetCityByCountryId(long Id)
        {
            try
            {
                var _Data = await _readjson.ReadJsonCity();
                var ByCountry = _Data.Data.City.Where(es => es.CountryId == Id).Select(es => new GeneralCity
                {
                    CityId = es.CityId,
                    CountryId = es.CountryId,
                    CityName = es.CityName

                }).ToList();
                return (false, "OK", ByCountry);
            }
            catch (Exception ex)
            {
                return (true, ex.Message, null);
            }
        }
    }
}
