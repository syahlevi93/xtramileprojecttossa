﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using BackEnd.Services;
using BackEnd.Responses;
namespace BackEnd.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CityController : ControllerBase
    {
        CityServices _cityservices = new CityServices();
        GeneralResponses _generalresponses = new GeneralResponses();

        [HttpGet("GetCountry")]
        public async Task<IActionResult> GetCity()
        {
            try
            {
                var Data = await _cityservices.GetCity();
                var Responses = new GeneralResponses()
                {
                    Error = Data.error,
                    Message = Data.message,
                    Data = Data.data
                };
                return Ok(Responses);
            }
            catch (Exception ex)
            {
                var Responses = new GeneralResponses()
                {
                    Error = true,
                    Message = ex.Message,
                    Data = null
                };
                return Ok(Responses);
            }
        }

        [HttpGet("GetCityByCountryId/{id}")]
        public async Task<IActionResult> GetCityByCounryId(long id)
        {
            try
            {
                var Data = await _cityservices.GetCityByCountryId(id);
                var Responses = new GeneralResponses()
                {
                    Error = Data.error,
                    Message = Data.message,
                    Data = Data.data
                };
                return Ok(Responses);
            }
            catch (Exception ex)
            {
                var Responses = new GeneralResponses()
                {
                    Error = true,
                    Message = ex.Message,
                    Data = null
                };
                return Ok(Responses);
            }
        }


        [HttpGet("GetWeatherByCity/{City}")]
        public async Task<IActionResult> GetWeatherByCity(string City)
        {
            try
            {
                var Data = await _cityservices.GetWeatherByCity(City);
                var Responses = new GeneralResponses()
                {
                    Error = Data.error,
                    Message = Data.message,
                    Data = Data.data
                };
                return Ok(Responses);
            }
            catch(Exception ex)
            {
                var Responses = new GeneralResponses()
                {
                    Error = true,
                    Message = ex.Message,
                    Data = null
                };
                return Ok(Responses);
            }

        }


       

    }
}

