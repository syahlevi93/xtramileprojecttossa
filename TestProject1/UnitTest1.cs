using BackEnd.Controllers;
using BackEnd.Services;

namespace TestProject1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public async Task Test1GetCountryController()
        {
            CityController _city = new CityController();
        var x= await _city.GetCity();
            Assert.IsNotNull(x);
        }

        [Test]
        public async Task Test1GetCityController()
        {
            CityController _city = new CityController();
            var x = await _city.GetCityByCounryId(1);
            Assert.IsNotNull(x);
        }
        [Test]
        public async Task Test1GetWeatherByCityController()
        {
            CityController _city = new CityController();
            var x = await _city.GetWeatherByCity("Jakarta");
            Assert.IsNotNull(x);
        }

        [Test]
        public async Task Test1GetCountryService()
        {
            CityServices _city = new CityServices();
            var x = await _city.GetCity();
            Assert.IsNotNull(x);
        }
        [Test]
        public async Task Test1GetCityService()
        {
            CityServices _city = new CityServices();
            var x = await _city.GetCityByCountryId(1);
            Assert.IsNotNull(x);
        }
        [Test]
        public async Task Test1GetWeatherService()
        {
            CityServices _city = new CityServices();
            var x = await _city.GetWeatherByCity("Jakarta");
            Assert.IsNotNull(x);
        }
    }
}