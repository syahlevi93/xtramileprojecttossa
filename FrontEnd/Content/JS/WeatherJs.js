﻿function GetCountry() {
    $.ajax({
        type: "GET",
        url: "http://localhost:5257/api/City/GetCountry",
        data: "{}",
        success: function (data) {
            if (data.error == false) {
                var s = '<option value="-1">Please Select a Country</option>';
                for (var i = 0; i < data.data.country.length; i++) {
                    s += '<option value="' + data.data.country[i].countryId + '">' + data.data.country[i].countryName + '</option>';
                }
                $("#Country").html(s);
            }
            else {
                alert("There is Something Wrong With The Service");
            }
        }
    });  

}

function GetCity() {
    $('#Country').on("change", function () {
        var Id = $('#Country').val();

        $.ajax({
            type: "GET",
            url: "http://localhost:5257/api/City/GetCityByCountryId/" + Id,
            data: "{}",
            success: function (data) {

                if (data.error == false) {
                    console.log(data.data);
                    var s = '<option value="-1">Please Select a City</option>';
                    for (var i = 0; i < data.data.length; i++) {
                        s += '<option value="' + data.data[i].cityName + '">' + data.data[i].cityName + '</option>';
                    }
                    $("#City").html(s);
                }
                else {
                    alert("There is Something Wrong With The Services");

                }
            }

           
        });
    });
}

function GenerateAPI(CityName) {

    $.ajax({
        type: "GET",
        url: "http://localhost:5257/api/City/GetWeatherByCity/" + CityName,
        data: "{}",
        success: function (data) {
            if (data.error == false) {
                console.log(data.data.location.country);
                $("#CountryCode").html(data.data.location.country);
                $("#CountryName").text(data.data.location.name);
                $("#Latitude").text(data.data.location.coord.lat);
                $("#Longtitude").text(data.data.location.coord.lon);

                $("#Alls").html(data.data.clouds.all);
                $("#Visibility").text(data.data.visibility);
                $("#Humidity").text(data.data.humidity);

                $("#Pressure").text(data.data.pressure);
                $("#Speed").text(data.data.wind.speed);
                $("#Deg").text(data.data.wind.deg);
                $("#Gust").text(data.data.wind.gust);
                $("#Humidity").text(data.data.humidity);

                $("#TempCel").text(data.data.temperature.tempCelcius);
                $("#TempFah").text(data.data.temperature.tempFahrenheit);
                $("#dated").text(data.data.time);

                $("#WeatherPred").text(data.data.weatherMain);
                $("#WeatherDesc").text(data.data.weatherDesc);
            }
            else {
                alert("There is Something Wrong With The Service");
            }
            //return data.data;
        }
    });


}

function Generate() {
    var CountryId = $('#Country').val();
    var CityId = $('#City').val();

    if (CountryId == -1 || CityId == -1) {
        alert("You Must Select A Country and A City");
    }
    else {
        GenerateAPI(CityId);
        
      
    }

}